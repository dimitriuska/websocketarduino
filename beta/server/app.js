/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var express = require('express');
var config = require('./config/environment');
// Setup server
var app = express();
var server = require('http').createServer(app);
require('./config/express')(app);
require('./routes')(app);

// Start server
server.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

// Expose app
exports = module.exports = app;

var serialport = require("serialport"); 
//var SerialPort = serialport.SerialPort; 
//var serialPort = new SerialPort("/dev/ttyACM0", {
 // baudrate: 115200,
 // parser: serialport.parsers.readline("\n")
//});


// use socket.io
var io = require('socket.io').listen(server);

io.on("connection", function(socket) {
     socket.on("tweetprocesson", function (tweet) {
        // we received a tweet from the browser
      console.log("/process?params="+tweet);
          serialPort.open(function (error) {
                          if ( error ) {
                              console.log('failed to open: '+error);
                                } else {
                             serialPort.write("/process?params="+tweet+"\n\r")};
                          });
                });


     socket.on("tweetprocessoff", function (tweet) {
        // we received a tweet from the browser
      console.log("/process?params="+tweet);
           serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/process?params="+tweet+"\n\r")};
                        });
     });
   
     socket.on("tweetTHAmax", function (tweet) {
        // we received a tweet from the browser
      console.log("/maxTHA?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/maxTHA?params="+tweet+"\n\r")};
                        });
     });

     socket.on("tweetTHAmin", function (tweet) {
        // we received a tweet from the browser
      console.log("/minTHA?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/minTHA?params="+tweet+"\n\r")};
                        });
     });

     socket.on("tweetTHBmax", function (tweet) {
        // we received a tweet from the browser
      console.log("/maxTHB?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/maxTHB?params="+tweet+"\n\r")};
                        });
     });
     
     socket.on("tweetTHBmin", function (tweet) {
        // we received a tweet from the browser
      console.log("/minTHB?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/minTHB?params="+tweet+"\n\r")};
                        });
     });


     socket.on("tweetresPWM", function (tweet) {
        // we received a tweet from the browser
      console.log("/resAPWM?params="+tweet+"\n\r");
     });

    
});


//serialPort.on("open", function () {
//  console.log('open');

//  serialPort.on('data', function(data) {
  //      console.log(data);
   //     io.sockets.emit('giornata', { 'some': data }); 
  //  });
//});












