#include <EEPROM.h>
#include "High_Temp.h"
#include <SPI.h>
#include <aREST.h>
#include <avr/wdt.h>

#define REGISTERS 11
byte registers[REGISTERS];

HighTemp htA(A3, A5);
HighTemp htB(A2, A4);

// start reading from the first byte (address 0) of the EEPROM
int address = 0;

// Create aREST instance
aREST rest = aREST();

//cycle duration 
unsigned long startA, finishedA, elapsedA, startB, finishedB, elapsedB;

// inputs 
boolean startprocess;
const int aventPin =  3;      //  vent1 pin
const int bventPin =  6;      // vent2 pin
const int aresPin =  5;      // res1 pin
const int bresPin =  9;      // res2 pin

// outputs
int tempA;
int tempB;
byte statusprocess;
byte counterA;
byte counterB;

// State Variables 
int maxthresholdA;
int maxthresholdB;
int minthresholdB;
int minthresholdA;
int cicliA;
int cicliB;
int heaterA;
int heaterB;
int ventA;
int ventB;
int resA;
int resB;

const int pushbuttonpinstart =  7;      // the number of the pushbutton pin
const int pushbuttonpinstop =  8;      // the number of the pushbutton pin

//set start = stop button high
int pushbuttonstartState;
int pushbuttonstopState;

int aventPinState = LOW;
int bventPinState = LOW;
int aresPinState = LOW;
int bresPinState = LOW;

/**
 * Divides a given PWM pin frequency by a divisor.
 * 
 * The resulting frequency is equal to the base frequency divided by
 * the given divisor:
 *   - Base frequencies:
 *      o The base frequency for pins 3, 9, 10, and 11 is 31250 Hz.
 *      o The base frequency for pins 5 and 6 is 62500 Hz.
 *   - Divisors:
 *      o The divisors available on pins 5, 6, 9 and 10 are: 1, 8, 64,
 *        256, and 1024.
 *      o The divisors available on pins 3 and 11 are: 1, 8, 32, 64,
 *        128, 256, and 1024.
 * 
 * PWM frequencies are tied together in pairs of pins. If one in a
 * pair is changed, the other is also changed to match:
 *   - Pins 5 and 6 are paired on timer0
 *   - Pins 9 and 10 are paired on timer1
 *   - Pins 3 and 11 are paired on timer2
 * 
 * Note that this function will have side effects on anything else
 * that uses timers:
 *   - Changes on pins 3, 5, 6, or 11 may cause the delay() and
 *     millis() functions to stop working. Other timing-related
 *     functions may also be affected.
 *   - Changes on pins 9 or 10 will cause the Servo library to function
 *     incorrectly.
 * 
 * Thanks to macegr of the Arduino forums for his documentation of the
 * PWM frequency divisors. His post can be viewed at:
 *   http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1235060559/0#4
 */
void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}



void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  
  
    
  // Function to be exposed
  rest.function("process",process);
  rest.function("minTHA",minTHA);
  rest.function("maxTHA",maxTHA);
  rest.function("minTHB",minTHB);
  rest.function("maxTHB",maxTHB);
  
  
  
  // Give name and ID to device
  rest.set_id("2");
  rest.set_name("serial");

  // Start watchdog
  wdt_enable(WDTO_4S);
   
  htA.begin();  
  htB.begin();
    
  registers[2]= EEPROM.read(2); 
  registers[3]= EEPROM.read(3);
  registers[6]= EEPROM.read(6);
  registers[7]= EEPROM.read(7);
    
  registers[8] = EEPROM.read(8);
  registers[9] = EEPROM.read(9);

  registers[10]=EEPROM.read(10);

  statusprocess=registers[10]; 
  maxthresholdA=registers[3]; 
  minthresholdA=registers[2];
  maxthresholdB=registers[7];
  minthresholdB=registers[6];
    
  counterA=registers[8];
  counterB=registers[9];
  
  cicliA=counterA/2;
  cicliB=counterB/2;

  heaterA = 1;
  heaterB = 1;
  
  startprocess = false;

  pinMode(aresPin, OUTPUT);
  pinMode(aventPin, OUTPUT); 
  pinMode(bresPin, OUTPUT);
  pinMode(bventPin, OUTPUT);
 
  pinMode(pushbuttonpinstart, INPUT);
  pinMode(pushbuttonpinstop, INPUT);
  
  // Set pin 3's PWM frequency to 1/10 Hz (312500/8 = 0.1)
    // Note that the base frequency for pins 3, 9, 10, and 11 is 31250 Hz
 //  setPwmFrequency(3, 312500);
 //  setPwmFrequency(9, 31250);

}


void loop() {
  
        // Handle REST calls
        rest.handle(Serial);  
        wdt_reset();

         tempA = htA.getThmc();
         tempB = htB.getThmc();
         ventA = analogRead(aventPin); 
         ventB = analogRead(bventPin);
         resA = analogRead(aresPin); 
         resB = analogRead(bresPin); 
         maxthresholdA = registers[3]; 
         minthresholdA = registers[2]; 
         maxthresholdB = registers[7]; 
         minthresholdB = registers[6];
         elapsedA=(finishedA-startA)/1000;
         elapsedB=(finishedB-startB)/1000; 
         sendJson();


if(statusprocess==0){
               
         digitalWrite(aresPin, aresPinState);
         digitalWrite(aventPin, aventPinState);
         digitalWrite(bresPin, bresPinState);
         digitalWrite(bventPin, bventPinState);
         if(digitalRead(pushbuttonpinstart)==LOW)
        {startprocess=true;}; 
            }

else{

         if(heaterA == 1){
             if(htA.getThmc()<registers[3]){heatingA();}
             else{heaterA=0;coolingA();counterA=counterA+1;cicliA=counterA;EEPROM.write(8,counterA);sendJson();startA=millis();}
                    } 
    else{
               if(htA.getThmc()>registers[2]){coolingA();}
             else{heaterA=1;heatingA();counterA=counterA+1;cicliA=counterA;EEPROM.write(8,counterA);sendJson();finishedA=millis();startA=0;}
                  }
         if(heaterB == 1){
             if(htB.getThmc()<registers[7]){heatingB();}
             else{heaterB=0;coolingB();counterB=counterB+1;cicliB=counterB;EEPROM.write(9,counterB);sendJson();startB=millis();}
                  } 
        else{
             if(htB.getThmc()>registers[6]){coolingB();}
             else{heaterB=1;coolingB();counterB=counterB+1;cicliB=counterB;EEPROM.write(9,counterB);sendJson();finishedB=millis();startB=0;}
                  }
        if(digitalRead(pushbuttonpinstop)==LOW) 
              {startprocess=false;}; 
         }


}

void heatingA() {
           digitalWrite(aresPin, HIGH);
           digitalWrite(aventPin, LOW);
           registers[8]=counterA;
}

void coolingA() {
           digitalWrite(aresPin, LOW);
           digitalWrite(aventPin, HIGH);
           registers[8]=counterA;
}        
       
void heatingB() {
           digitalWrite(bresPin, HIGH);
           digitalWrite(bventPin, LOW);
           registers[9]=counterB;
 }
                
void coolingB() {
           digitalWrite(bresPin, LOW);
           digitalWrite(bventPin, HIGH);
           registers[9]=counterB;                        
}           

// Custom function accessible by the API
int process(String command) {
  
  // Get state from command
  int state = command.toInt();
  if(state==1){startprocess==true;statusprocess=1;}
  else{startprocess==false;statusprocess=0;};
  registers[10]=statusprocess;
  EEPROM.write(10, registers[10]);
  
  return statusprocess;
}
           

int minTHA(String command) {
  
  // Get state from command
  int state = command.toInt();
  minthresholdA = state;
  registers[2]=state;
  EEPROM.write(2, registers[2]);
  return state;
}           
           
int maxTHA(String command) {
  
  // Get state from command
  int state = command.toInt();
  maxthresholdA = state;
  registers[3]=state;
  EEPROM.write(3, registers[3]);
  return state;
}              

int minTHB(String command) {
  
  // Get state from command
  int state = command.toInt();
  registers[6]=state;
  EEPROM.write(6, registers[6]);
  
  //digitalWrite(6,state);
  return state;
}           
           
int maxTHB(String command) {
  
  // Get state from command
  int state = command.toInt();
  registers[7]=state;
  EEPROM.write(7, registers[7]);
  
  //digitalWrite(6,state);
  return state;
}  

void sendJson(){
    String json;
    
  //  json = "{\"variable\":{\"tempA\":";
    json = "{\"Process\":\"";
    json += statusprocess;
    json += "\",\"cicliA\":\"";
    json += cicliA;
    json += "\",\"cicliB\":\"";
    json += cicliB;
    json += "\",\"elapsedA\":\"";
    json += elapsedA;
    json += "\",\"elapsedB\":\"";
    json += elapsedB;
    json += "\",\"tempA\":\"";
    json += tempA;
    json += "\",\"tempB\":\"";
    json += tempB;
    json += "\",\"heaterA\":\"";
    json += heaterA;
    json += "\",\"heaterB\":\"";
    json += heaterB;
    json += "\",\"minthresholdA\":\"";
    json += minthresholdA;
    json += "\",\"maxthresholdA\":\"";
    json += maxthresholdA;
    json += "\",\"minthresholdB\":\"";
    json += minthresholdB;
    json += "\",\"maxthresholdB\":\"";
    json += maxthresholdB;
    json += "\",\"resA\":\"";
    json += resA;
    json += "\",\"resB\":\"";
    json += resB;
    json += "\",\"ventA\":\"";
    json += ventA;
    json += "\",\"ventB\":\"";
    json += ventB;
    json += "\"}";
   

    Serial.println(json);
     delay(1000);
}
