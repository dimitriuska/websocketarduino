var http = require('http');
var url = require('url');
var fs = require('fs');

var serialport = require("serialport"); 
var SerialPort = serialport.SerialPort; 
var serialPort = new SerialPort("/dev/ttyACM0", {
  baudrate: 115200,
  parser: serialport.parsers.readline("\n")
});

var server;

server = http.createServer(function(req, res){
    // your normal server code
    var path = url.parse(req.url).pathname;
    switch (path){
        case '/':
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write('<h1>Hello! <a href="/client-1.1.html">Test VANZI</a></h1>');
            res.end();
            break;
        case '/client-1.1.html':
            fs.readFile(__dirname + path, function(err, data){
                if (err){ 
                    return send404(res);
                }
                res.writeHead(200, {'Content-Type': path == 'json.js' ? 'text/javascript' : 'text/html'});
                res.write(data, 'utf8');
                res.end();
            });    
        break;
        default: send404(res);
    }
}),

send404 = function(res){
    res.writeHead(404);
    res.write('404');
    res.end();
};


server.listen(8001);

// use socket.io
var io = require('socket.io').listen(server);

io.on("connection", function(socket) {
     socket.on("tweetprocesson", function (tweet) {
        // we received a tweet from the browser
      console.log("/process?params="+tweet);
          serialPort.open(function (error) {
                          if ( error ) {
                              console.log('failed to open: '+error);
                                } else {
                             serialPort.write("/process?params="+tweet+"\n\r")};
                          });
                });


     socket.on("tweetprocessoff", function (tweet) {
        // we received a tweet from the browser
      console.log("/process?params="+tweet);
           serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/process?params="+tweet+"\n\r")};
                        });
     });
   
     socket.on("tweetTHAmax", function (tweet) {
        // we received a tweet from the browser
      console.log("/maxTHA?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/maxTHA?params="+tweet+"\n\r")};
                        });
     });

     socket.on("tweetTHAmin", function (tweet) {
        // we received a tweet from the browser
      console.log("/minTHA?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/minTHA?params="+tweet+"\n\r")};
                        });
     });

     socket.on("tweetTHBmax", function (tweet) {
        // we received a tweet from the browser
      console.log("/maxTHB?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/maxTHB?params="+tweet+"\n\r")};
                        });
     });
     
     socket.on("tweetTHBmin", function (tweet) {
        // we received a tweet from the browser
      console.log("/minTHB?params="+tweet+"\n\r");
          serialPort.open(function (error) {
                         if ( error ) {
                         console.log('failed to open: '+error);
                              } else {
                            serialPort.write("/minTHB?params="+tweet+"\n\r")};
                        });
     });


     socket.on("tweetresPWM", function (tweet) {
        // we received a tweet from the browser
      console.log("/resAPWM?params="+tweet+"\n\r");
     });

    
});


serialPort.on("open", function () {
//  console.log('open');

  serialPort.on('data', function(data) {
        console.log(data);
        io.sockets.emit('giornata', { 'some': data }); 
    });
});
